from unittest import TestCase
from unittest.mock import Mock, patch

from sila2_driver.zaber.motorized_asr.api.zaber import ZaberConnection as Zaber


class TestZaberApi(TestCase):
    def setUp(self) -> None:
        self.zaber = Zaber(simulation_active=True)

    def test_connect(self):
        """Make sure internal connection variable is set after connect()"""
        with self.zaber.connect() as conn:
            self.assertIsNotNone(conn)
            conn.check_open_ex()  # Test connection is open
        self.assertRaises(Exception, conn.check_open_ex)  # Test connection is closed

    def test_connect_nested(self):
        """Make sure internal connection variable is set after nested connect()"""
        with self.zaber.connect() as conn1:
            conn1.check_open_ex()  # Test connection 1 is open
            with self.zaber.connect() as conn2:
                conn1.check_open_ex()  # Test connection 1 is still open
                conn2.check_open_ex()  # Test connection 2 is open
            conn1.check_open_ex()  # Test connection 1 is still open
            self.assertRaises(Exception, conn2.check_open_ex)  # Test connection 2 is closed
        self.assertRaises(Exception, conn1.check_open_ex)  # Test connection 1 is closed

    def test_move(self):
        with self.zaber.connect() as conn:
            positions = [(1, 1), (-2, 2), (3, -3), (-4, -4), (5, 5)]
            names = [str(x) for x in positions]

            for n, p in zip(names, positions):
                with self.subTest(msg=n):
                    conn.move_to(*p)
                    r = conn.get_position()
                    self.assertEqual(r[0], p[0])
                    self.assertEqual(r[1], p[1])

    @patch("sila2_driver.zaber.motorized_asr.api.zaber_dummy.DummyAxis.stop")
    def test_stop(self, mock: Mock):
        with self.zaber.connect() as conn:
            conn.stop()
            self.assertEqual(mock.call_count, len(conn.get_position()))  # One for each axis

    @patch("sila2_driver.zaber.motorized_asr.api.zaber_dummy.DummyAllAxes.home")
    def test_home(self, mock: Mock):
        with self.zaber.connect() as conn:
            conn.home()
            mock.assert_called_once()

    @patch("sila2_driver.zaber.motorized_asr.api.zaber_dummy.DummyAllAxes.park")
    def test_park(self, mock: Mock):
        with self.zaber.connect() as conn:
            conn.park()
            mock.assert_called_once()

    @patch("sila2_driver.zaber.motorized_asr.api.zaber_dummy.DummyIO.set_digital_output")
    def test_set_output(self, mock: Mock):
        with self.zaber.connect() as conn:
            conn.set_digital_output(1, True)
            mock.assert_called_once_with(1, True)

    @patch("sila2_driver.zaber.motorized_asr.api.zaber_dummy.DummyAllAxes.unpark")
    def test_unpack(self, mock: Mock):
        with self.zaber.connect() as conn:
            conn.unpark()
            mock.assert_called_once()
