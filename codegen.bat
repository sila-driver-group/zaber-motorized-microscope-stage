@echo off

set TARGET_FOLDER=.\src\sila2_driver\zaber\motorized_asr\generated\

set XML1=.\feature_xml\CancelController-v1_0.sila.xml
set XML2=.\feature_xml\Settings-v1_0.sila.xml
set XML3=.\feature_xml\SimulationController-v1_0.sila.xml
set XML4=.\feature_xml\StageController-v1_0.sila.xml

@echo #Generating Sila2 dependencies
@echo Target folder: %TARGET_FOLDER%

sila2-codegen.exe generate-feature-files --overwrite -o %TARGET_FOLDER% %XML1% %XML2% %XML3% %XML4%