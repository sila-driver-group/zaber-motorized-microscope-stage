<?xml version="1.0" encoding="utf-8"?>
<Feature SiLA2Version="1.0" FeatureVersion="1.0" MaturityLevel="Verified"
  Originator="org.silastandard"
  Category="examples"
  xmlns="http://www.sila-standard.org"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.sila-standard.org https://gitlab.com/SiLA2/sila_base/raw/master/schema/FeatureDefinition.xsd">
  <Identifier>StageController</Identifier>
  <DisplayName>Microscope Stage Controller</DisplayName>
  <Description>Feature to control a microscope stage</Description>

  <Property>
    <Identifier>Position</Identifier>
    <DisplayName>Current Position</DisplayName>
    <Description>Current Position</Description>
    <Observable>No</Observable>
    <DataType>
      <DataTypeIdentifier>PositionXY</DataTypeIdentifier>
    </DataType>
    <DefinedExecutionErrors>
      <Identifier>IOError</Identifier>
    </DefinedExecutionErrors>
  </Property>

  <Command>
    <Identifier>Home</Identifier>
    <DisplayName>Move To Home</DisplayName>
    <Description>Move the stage to the home position and reset encoders</Description>
    <Observable>Yes</Observable>
    <DefinedExecutionErrors>
      <Identifier>CancelledError</Identifier>
      <Identifier>IOError</Identifier>
      <Identifier>TimeoutError</Identifier>
    </DefinedExecutionErrors>
  </Command>

  <Command>
    <Identifier>MoveTo</Identifier>
    <DisplayName>Move To Position</DisplayName>
    <Description>Move the stage to a specific position</Description>
    <Observable>Yes</Observable>
    <Parameter>
      <Identifier>Position</Identifier>
      <DisplayName>Position</DisplayName>
      <Description>Position to go to</Description>
      <DataType>
        <DataTypeIdentifier>PositionXY</DataTypeIdentifier>
      </DataType>
    </Parameter>
    <Response>
      <Identifier>Position</Identifier>
      <DisplayName>End Position</DisplayName>
      <Description>End position</Description>
      <DataType>
        <DataTypeIdentifier>PositionXY</DataTypeIdentifier>
      </DataType>
    </Response>
    <IntermediateResponse>
      <Identifier>Position</Identifier>
      <DisplayName>Current Position</DisplayName>
      <Description>Current position</Description>
      <DataType>
        <DataTypeIdentifier>PositionXY</DataTypeIdentifier>
      </DataType>
    </IntermediateResponse>
    <DefinedExecutionErrors>
      <Identifier>CancelledError</Identifier>
      <Identifier>IOError</Identifier>
      <Identifier>TimeoutError</Identifier>
    </DefinedExecutionErrors>
  </Command>

  <Command>
    <Identifier>Park</Identifier>
    <DisplayName>Park the Stage</DisplayName>
    <Description>Park the stage in preparation for powering off device</Description>
    <Observable>No</Observable>
    <DefinedExecutionErrors>
      <Identifier>IOError</Identifier>
    </DefinedExecutionErrors>
  </Command>

  <Command>
    <Identifier>SetOutput</Identifier>
    <DisplayName>Set Output</DisplayName>
    <Description>Set stage digital output</Description>
    <Observable>No</Observable>
    <Parameter>
      <Identifier>Channel</Identifier>
      <DisplayName>Channel</DisplayName>
      <Description>Output channel to set</Description>
      <DataType>
        <Constrained>
          <DataType>
            <Basic>Integer</Basic>
          </DataType>
          <Constraints>
            <MinimalInclusive>0</MinimalInclusive>
          </Constraints>
        </Constrained>
      </DataType>
    </Parameter>
    <Parameter>
      <Identifier>Value</Identifier>
      <DisplayName>Value</DisplayName>
      <Description>Value to write to output channel</Description>
      <DataType>
        <Basic>Boolean</Basic>
      </DataType>
    </Parameter>
    <DefinedExecutionErrors>
      <Identifier>IOError</Identifier>
    </DefinedExecutionErrors>
  </Command>

  <Command>
    <Identifier>Unpark</Identifier>
    <DisplayName>Unpark the Stage</DisplayName>
    <Description>Unpark the stage, required to start operation</Description>
    <Observable>No</Observable>
    <DefinedExecutionErrors>
      <Identifier>IOError</Identifier>
    </DefinedExecutionErrors>
  </Command>

  <DataTypeDefinition>
    <Identifier>PositionXY</Identifier>
    <DisplayName>PositionXY</DisplayName>
    <Description>Position in X and Y direction</Description>
    <DataType>
      <Structure>
        <Element>
          <Identifier>X</Identifier>
          <DisplayName>X</DisplayName>
          <Description>Postion in X direction</Description>
          <DataType>
            <Constrained>
              <DataType>
                <Basic>Real</Basic>
              </DataType>
              <Constraints>
                <Unit>
                  <Label>mm</Label>
                  <Factor>-3</Factor>
                  <Offset>0</Offset>
                  <UnitComponent>
                    <SIUnit>Meter</SIUnit>
                    <Exponent>1</Exponent>
                  </UnitComponent>
                </Unit>
              </Constraints>
            </Constrained>
          </DataType>
        </Element>
        <Element>
          <Identifier>Y</Identifier>
          <DisplayName>Y</DisplayName>
          <Description>Postion in Y direction</Description>
          <DataType>
            <Constrained>
              <DataType>
                <Basic>Real</Basic>
              </DataType>
              <Constraints>
                <Unit>
                  <Label>mm</Label>
                  <Factor>-3</Factor>
                  <Offset>0</Offset>
                  <UnitComponent>
                    <SIUnit>Meter</SIUnit>
                    <Exponent>1</Exponent>
                  </UnitComponent>
                </Unit>
              </Constraints>
            </Constrained>
          </DataType>
        </Element>
      </Structure>
    </DataType>
  </DataTypeDefinition>

  <!-- Errors -->
  <DefinedExecutionError>
    <Identifier>CancelledError</Identifier>
    <DisplayName>Cancelled Error</DisplayName>
    <Description>Call was cancelled by user</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>IOError</Identifier>
    <DisplayName>IO Error</DisplayName>
    <Description>IO Error</Description>
  </DefinedExecutionError>
  <DefinedExecutionError>
    <Identifier>TimeoutError</Identifier>
    <DisplayName>Timeout Error</DisplayName>
    <Description>End was not reached on time</Description>
  </DefinedExecutionError>

</Feature>