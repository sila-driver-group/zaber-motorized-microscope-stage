from __future__ import annotations

import time
from threading import Lock

from zaber_motion import Units
from zaber_motion.ascii import Connection as Zaber

from .zaber_dummy import DummyConnection as DummyZaber


class ZaberConnection:
    @property
    def port_name(self) -> str:
        return self.__port_name

    @port_name.setter
    def port_name(self, value: str):
        self.__port_name = value

    @property
    def simulation_active(self) -> bool:
        return self.__simulation_active

    @simulation_active.setter
    def simulation_active(self, value: bool):
        self.__simulation_active = value

    def __init__(self, *, port_name=None, simulation_active=False) -> None:
        self.simulation_active = simulation_active
        self.__connection_counter = 0
        self.__counter_lock = Lock()
        self.__io_lock = Lock()
        self.__move_lock = Lock()
        self.__connection = None
        self.__devices = []
        self.__port_name = port_name

    def connect(self, *, portName: str = None, simulation: bool = None) -> ZaberHandle:
        if portName is not None:
            self.port_name = portName
        if simulation is not None:
            self.simulation_active = simulation
        return ZaberHandle(self)

    def register(self) -> None:
        with self.__counter_lock:
            if self.__connection_counter != 0:
                self.__connection_counter += 1
            else:
                try:
                    self.__connection = (
                        DummyZaber.open_serial_port()
                        if self.simulation_active
                        else Zaber.open_serial_port(self.port_name, direct=True)
                    )
                    self.__devices = self.__connection.detect_devices()
                except Exception as e:
                    raise e
                self.__connection_counter += 1

    def unregister(self) -> None:
        with self.__counter_lock:
            self.__connection_counter -= 1
            if self.__connection_counter == 0:
                self.__devices = []
                if self.__connection:
                    try:
                        self.__connection.close()
                    finally:
                        self.__connection = None

    def stop(self) -> None:
        with self.__io_lock:
            self.__devices[0].get_axis(1).stop()
            self.__devices[0].get_axis(2).stop()

    def get_position(self) -> tuple[float, float]:
        with self.__io_lock:
            return (
                self.__devices[0].get_axis(1).get_position(Units.LENGTH_MILLIMETRES),
                self.__devices[0].get_axis(2).get_position(Units.LENGTH_MILLIMETRES),
            )

    def home(self, wait: bool = True) -> None:
        try:
            locked = self.__move_lock.acquire(timeout=0.1)  # Wait a bit if needed
            if not locked:
                raise Exception("Unable to get Move Lock")
            with self.__io_lock:
                self.__devices[0].all_axes.home(wait_until_idle=False)
            if wait:
                self.__wait_for_not_busy()

        finally:
            if locked:
                self.__move_lock.release()

    def is_busy(self) -> bool:
        with self.__io_lock:
            return self.__devices[0].get_axis(1).is_busy() or self.__devices[0].get_axis(2).is_busy()

    def is_homed(self) -> bool:
        with self.__io_lock:
            return self.__devices[0].get_axis(1).is_homed() and self.__devices[0].get_axis(2).is_homed()

    def move_to(self, x, y, wait: bool = True, velocity: float = 20, acceleration: float = 20) -> None:
        try:
            locked = self.__move_lock.acquire(timeout=0.1)  # Wait a bit if needed
            if not locked:
                raise Exception("Unable to get Move Lock")
            with self.__io_lock:
                self.__devices[0].get_axis(1).move_absolute(
                    x,
                    Units.LENGTH_MILLIMETRES,
                    wait_until_idle=False,
                    velocity=velocity,
                    velocity_unit=Units.VELOCITY_MILLIMETRES_PER_SECOND,
                    acceleration=acceleration,
                    acceleration_unit=Units.ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED,
                )
                self.__devices[0].get_axis(2).move_absolute(
                    y,
                    Units.LENGTH_MILLIMETRES,
                    wait_until_idle=False,
                    velocity=velocity,
                    velocity_unit=Units.VELOCITY_MILLIMETRES_PER_SECOND,
                    acceleration=acceleration,
                    acceleration_unit=Units.ACCELERATION_MILLIMETRES_PER_SECOND_SQUARED,
                )
            if wait:
                self.__wait_for_not_busy()

        finally:
            if locked:
                self.__move_lock.release()

    def park(self) -> None:
        try:
            locked = self.__move_lock.acquire(timeout=0.1)  # Wait a bit if needed
            with self.__io_lock:
                self.__devices[0].all_axes.park()
        finally:
            if locked:
                self.__move_lock.release()

    def set_output(self, output: int, value: bool) -> None:
        with self.__io_lock:
            self.__devices[0].io.set_digital_output(output, value)

    def unpark(self) -> None:
        try:
            locked = self.__move_lock.acquire(timeout=0.1)  # Wait a bit if needed
            with self.__io_lock:
                self.__devices[0].all_axes.unpark()
        finally:
            if locked:
                self.__move_lock.release()

    def __wait_for_not_busy(self):
        while True:
            with self.__io_lock:
                busy = self.__devices[0].get_axis(1).is_busy() or self.__devices[0].get_axis(2).is_busy()
            if not busy:
                break


class ZaberHandle:
    def __init__(self, zaber: ZaberConnection) -> None:
        self.__zaber = zaber
        self.__open = False

    def __enter__(self):
        self.__zaber.register()
        self.__open = True
        return self

    def __exit__(self, exc_type, exc_value, traceback) -> None:
        self.__zaber.unregister()
        self.__open = False

    def check_open_ex(self) -> None:
        """Check if the connection is open. Raises an exception if not.

        Raises:
            Exception: Connection not open
        """
        if not self.__open:
            raise Exception("This method can not be run outside a `with` statement.")

    def stop(self):
        self.check_open_ex()
        self.__zaber.stop()

    def get_position(self):
        self.check_open_ex()
        return self.__zaber.get_position()

    def home(self, wait: bool = True) -> None:
        self.check_open_ex()
        self.__zaber.home(wait)

    def is_busy(self) -> bool:
        self.check_open_ex()
        return self.__zaber.is_busy()

    def is_homed(self) -> bool:
        self.check_open_ex()
        return self.__zaber.is_homed()

    def move_to(self, x: float, y: float, wait: bool = True, velocity: float = 20, acceleration: float = 20) -> None:
        self.check_open_ex()
        self.__zaber.move_to(x, y, wait, velocity, acceleration)

    def park(self) -> None:
        self.check_open_ex()
        self.__zaber.park()

    def set_digital_output(self, output: int, value: bool) -> None:
        self.check_open_ex()
        self.__zaber.set_output(output, value)

    def unpark(self) -> None:
        self.check_open_ex()
        self.__zaber.unpark()
