import logging

logger = logging.getLogger(__name__)


class DummyConnection:
    def __init__(self, **kwargs) -> None:
        self.__devices = [DummyDevice(self)]
        self.__open = True

    def open_serial_port(*args, **kwargs):
        logger.debug("Opening serial port")
        return DummyConnection()

    def detect_devices(self):
        if not self.__open:
            raise Exception("Connection is closed")
        return [*self.__devices]

    def close(self):
        logger.debug("Closing serial port")
        self.__open = False


class DummyDevice:
    def __init__(self, parent: DummyConnection) -> None:
        self.__axis = [DummyAxis(self, 1), DummyAxis(self, 2)]
        self.parent = parent
        self.position = [0, 0]
        self.all_axes = DummyAllAxes(self)
        self.io = DummyIO()

    def get_axis(self, axis: int):
        return self.__axis[axis - 1]


class DummyAxis:
    def __init__(self, parent: DummyDevice, idx: int) -> None:
        self.parent = parent
        self.idx = idx

    def get_position(self, unit):
        return self.parent.position[self.idx - 1]

    def move_absolute(
        self,
        position,
        unit,
        wait_until_idle=True,
        velocity=20,
        velocity_unit=None,
        acceleration=20,
        acceleration_unit=None,
    ):
        logger.debug(f"Moving axis {self.idx} from {self.parent.position[self.idx - 1]} to {position}")
        self.parent.position[self.idx - 1] = position

    def is_busy(self):
        return False

    def is_homed(self):
        return True

    def stop(self):
        logger.debug("Stopping")


class DummyAllAxes:
    def __init__(self, parent: DummyDevice) -> None:
        self.parent = parent

    def home(self, wait_until_idle=True):
        logger.debug("Homing")
        self.parent.position = [0, 0]

    def park(self):
        logger.debug("Parking")

    def unpark(self):
        logger.debug("Unparking")


class DummyIO:
    def set_digital_output(self, channel_number: int, value: bool):
        logger.debug(f"Setting output {channel_number} to {value}")
