# Generated by sila2.code_generator; sila2.__version__: 0.10.5
from __future__ import annotations

from typing import TYPE_CHECKING

from sila2.server import MetadataDict

from ..generated.settingsservice import SetSerialPort_Responses, SettingsServiceBase

if TYPE_CHECKING:
    from ..server import Server


class SettingsServiceImpl(SettingsServiceBase):
    # Static variable to store the port, for application wide access
    Props = {"port": ""}

    def get_SerialPort(self, *, metadata: MetadataDict) -> str:
        return SettingsServiceImpl.Props["port"]

    def SetSerialPort(self, PortName: str, *, metadata: MetadataDict) -> SetSerialPort_Responses:
        SettingsServiceImpl.Props["port"] = PortName
        return SetSerialPort_Responses()
