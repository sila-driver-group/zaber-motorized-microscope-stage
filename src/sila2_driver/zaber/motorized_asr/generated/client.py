# Generated by sila2.code_generator; sila2.__version__: 0.10.5
from __future__ import annotations

from typing import Set

from sila2.client import SilaClient
from sila2.framework import FullyQualifiedFeatureIdentifier

from . import cancelcontroller, settingsservice, simulationcontroller, stagecontroller


class Client(SilaClient):
    CancelController: cancelcontroller.CancelControllerClient

    SettingsService: settingsservice.SettingsServiceClient

    SimulationController: simulationcontroller.SimulationControllerClient

    StageController: stagecontroller.StageControllerClient

    _expected_features: Set[FullyQualifiedFeatureIdentifier] = {
        FullyQualifiedFeatureIdentifier("org.silastandard/core/SiLAService/v1"),
        FullyQualifiedFeatureIdentifier("org.silastandard/core.commands/CancelController/v1"),
        FullyQualifiedFeatureIdentifier("org.silastandard/examples/SettingsService/v1"),
        FullyQualifiedFeatureIdentifier("org.silastandard/core/SimulationController/v1"),
        FullyQualifiedFeatureIdentifier("org.silastandard/examples/StageController/v1"),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._register_defined_execution_error_class(
            cancelcontroller.CancelControllerFeature.defined_execution_errors["InvalidCommandExecutionUUID"],
            cancelcontroller.InvalidCommandExecutionUUID,
        )

        self._register_defined_execution_error_class(
            cancelcontroller.CancelControllerFeature.defined_execution_errors["OperationNotSupported"],
            cancelcontroller.OperationNotSupported,
        )

        self._register_defined_execution_error_class(
            simulationcontroller.SimulationControllerFeature.defined_execution_errors["StartSimulationModeFailed"],
            simulationcontroller.StartSimulationModeFailed,
        )

        self._register_defined_execution_error_class(
            simulationcontroller.SimulationControllerFeature.defined_execution_errors["StartRealModeFailed"],
            simulationcontroller.StartRealModeFailed,
        )

        self._register_defined_execution_error_class(
            stagecontroller.StageControllerFeature.defined_execution_errors["CancelledError"],
            stagecontroller.CancelledError,
        )

        self._register_defined_execution_error_class(
            stagecontroller.StageControllerFeature.defined_execution_errors["IOError"],
            stagecontroller.IOError,
        )

        self._register_defined_execution_error_class(
            stagecontroller.StageControllerFeature.defined_execution_errors["TimeoutError"],
            stagecontroller.TimeoutError,
        )
